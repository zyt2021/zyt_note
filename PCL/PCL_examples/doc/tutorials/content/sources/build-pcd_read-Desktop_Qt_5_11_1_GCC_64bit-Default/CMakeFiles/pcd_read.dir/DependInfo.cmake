# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/yumi/Project/PCL/PCL_examples/doc/tutorials/content/sources/pcd_read/pcd_read.cpp" "/home/yumi/Project/PCL/PCL_examples/doc/tutorials/content/sources/build-pcd_read-Desktop_Qt_5_11_1_GCC_64bit-Default/CMakeFiles/pcd_read.dir/pcd_read.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "DISABLE_LIBUSB_1_0"
  "DISABLE_PCAP"
  "DISABLE_PNG"
  "QT_CORE_LIB"
  "QT_GUI_LIB"
  "QT_NO_DEBUG"
  "QT_WIDGETS_LIB"
  "qh_QHpointer"
  "vtkDomainsChemistry_AUTOINIT=1(vtkDomainsChemistryOpenGL2)"
  "vtkRenderingContext2D_AUTOINIT=1(vtkRenderingContextOpenGL2)"
  "vtkRenderingCore_AUTOINIT=3(vtkInteractionStyle,vtkRenderingFreeType,vtkRenderingOpenGL2)"
  "vtkRenderingOpenGL2_AUTOINIT=1(vtkRenderingGL2PSOpenGL2)"
  "vtkRenderingVolume_AUTOINIT=1(vtkRenderingVolumeOpenGL2)"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "/usr/local/include/vtk-7.1"
  "/usr/local/include/pcl-1.9"
  "/usr/include/eigen3"
  "/usr/include/ni"
  "/usr/include/openni2"
  "/usr/local/include"
  "/opt/Qt5.11.1/5.11.1/gcc_64/include"
  "/opt/Qt5.11.1/5.11.1/gcc_64/include/QtWidgets"
  "/opt/Qt5.11.1/5.11.1/gcc_64/include/QtGui"
  "/opt/Qt5.11.1/5.11.1/gcc_64/include/QtCore"
  "/opt/Qt5.11.1/5.11.1/gcc_64/./mkspecs/linux-g++"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
