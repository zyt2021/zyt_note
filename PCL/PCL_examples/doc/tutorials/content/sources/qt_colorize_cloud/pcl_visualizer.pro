#-------------------------------------------------
#
# Project created by QtCreator 2014-11-11T14:00:00
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = colorize_cloud
TEMPLATE = app


SOURCES += main.cpp\
        pclviewer.cpp

HEADERS  += pclviewer.h

FORMS    += pclviewer.ui

INCLUDEPATH+=/usr/local/include/pcl-1.9\
/usr/include/eigen3\
/usr/local/include/vtk-7.1

LIBS+=/usr/local/lib/libpcl_*.so\
-lrt\
-lboost_system
LIBS += -L"/usr/local/lib"
LIBS += -lvtkCommonDataModel-7.1
LIBS += -lvtkCommonCore-7.1
LIBS += -lvtkCommonMath-7.1
LIBS += -lvtkRenderingCore-7.1\
-lQVTK\
-lvtkQtChart

