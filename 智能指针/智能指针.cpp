class A
{
public:
    A(){}
    ~A(){}
    void print(){std::cout<<"print\n";}
};

std::shared_ptr<A> getA(const std::shared_ptr<A> &a)
{
    std::cout<<"函数内引用计数: "<<a.use_count()<<std::endl;
    return a;
}

int main()
{
    //右值引用和std::move
    //1.防止A a = A();  A()分配内存，a拷贝构造，再次分配内存，A()释放。 **直接把A()分配的内存给a就行了，他的资源指针给null,不再访问
    //2.std::move 将a强转右值引用，此时a的空间和b共享，但再构造函数中会删除a对资源的访问。
    //3.A a; A b; a = b; 同样，a会先释放，再分配。如果往后，b的资源不再使用，那么就可以用 a = std::move(b)即可，b资源指针给null,不再访问

    std::weak_ptr<A> d;        //引用计数不加1,只是存储了b的引用过,在用的时候可以判断引用对象是否还存在

    {
        std::unique_ptr<A> a(new A()); //只是为了用在不想自己手动释放资源的地方,比如某个类中的私有指针变量,不会被别人调用,则就可以使用,不用delete
        //std::unique_ptr<A> a0 = a; 错误
        std::cout<<"a的状态: "<<(a==nullptr)<<std::endl;
        std::unique_ptr<A> a0  = std::move(a); //把a的地址内容权限全部交给a0
        std::cout<<"a的状态: "<<(a==nullptr)<<std::endl;

        std::shared_ptr<A> b(new A()); //在别的地方,或者别的类中需要访问, 则需要shared_ptr
        std::cout<<"引用计数: "<<b.use_count()<<std::endl;

        auto b0 = getA(b);

        {
            std::shared_ptr<A> c = b;      //shared_ptr引用计数+1
            std::cout<<"引用计数: "<<b.use_count()<<std::endl;
        }

        std::cout<<"引用计数: "<<b.use_count()<<std::endl;
        d = b;                            //weak_ptr
        std::cout<<"引用计数: "<<b.use_count()<<std::endl;
        b->print();
        if(d.expired())
        {
            std::cout<<"1已经无效"<<std::endl;
        }
    }

    if(d.expired())
    {
        std::cout<<"2已经无效"<<std::endl;
    }
    else
    {
        d.lock()->print(); //使用方法
    }
}

输出:

a的状态: 0
a的状态: 1
引用计数: 1
函数内引用计数: 1
引用计数: 3
引用计数: 2
引用计数: 2
print
2已经无效
