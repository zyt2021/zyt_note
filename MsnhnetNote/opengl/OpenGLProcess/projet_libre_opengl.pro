#-------------------------------------------------
#
# Project created by QtCreator 2014-08-03T10:43:51
#
#-------------------------------------------------

QT       += core gui
QT       += opengl

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = projet_libre_opengl
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    mainpanel.cpp

HEADERS  += mainwindow.h \
    mainpanel.h \
    observable.h \
    observer.h

FORMS    += mainwindow.ui

RESOURCES += \
    shaders.qrc

CONFIG += c++11

INCLUDEPATH += D:/libs/glew-2.1.0/include
LIBS+= D:/libs/glew-2.1.0/lib/Release/x64/glew32.lib\
$$quote(C:/Program Files (x86)/Windows Kits/10/Lib/10.0.17763.0/um/x64/OpenGL32.Lib)
