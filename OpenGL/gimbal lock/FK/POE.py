from sympy import *
import numpy as np

eye = np.eye(3)

def wedge(omg):
    w = [0, -omg[2],omg[1],\
        omg[2],   0,-omg[0],\
       -omg[1],omg[0],   0]
    return w

def calT(w,v,t):
    R = Matrix(eye) + sin(t)*Matrix(wedge(w)).reshape(3,3) + (1-cos(t))*Matrix(wedge(w)).reshape(3,3)*Matrix(wedge(w)).reshape(3,3)
    p = (Matrix(eye)*t + (1-cos(t))*Matrix(wedge(w)).reshape(3,3) + (t-sin(t))*Matrix(wedge(w)).reshape(3,3)*Matrix(wedge(w)).reshape(3,3))*(Matrix(v).reshape(3,1))
    T = np.vstack((np.hstack((R,p)),np.array((0,0,0,1))))
    return Matrix(T)

def Adj(T):
    R = Matrix([T[0,0],T[0,1],T[0,2],\
                T[1,0],T[1,1],T[1,2],\
                T[2,0],T[2,1],T[2,2]\
              ]).reshape(3,3)
    p = Matrix([T[0,3],T[1,3],T[2,3]]).reshape(1,3)

    zero = Matrix([0,0,0,0,0,0,0,0,0]).reshape(3,3)

    Tu = np.hstack((R, Matrix(wedge(p)).reshape(3,3)*R))
    Td = np.hstack((zero,R))
    Tf = np.vstack((Tu,Td))
    return Tf

def jacob(T,v,w):
    J = Adj(T)
    return J*Matrix([v[0],v[1],v[2],w[0],w[1],w[2]]).reshape(6,1)

init_printing()

def cal3Link():
    t3 = Symbol("t3") #theta 3
    t2 = Symbol("t2") #theta 2
    t1 = Symbol("t1") #theta 2

    a  = Symbol("a")  #a
    b  = Symbol("b")  #b

    M = Matrix([1, 0, 0, a+b, \
                0, 1, 0, 0,   \
                0, 0, 1, 0, \
                0, 0, 0, 1]).reshape(4,4)

    #Space Frame
    w3 = [0,0,1]
    v3 = [0,-a,0]

    w2 = [0,0,0]
    v2 = [1,0,0]

    w1 = [0,0,1]
    v1 = [0,0,0]

    pprint(trigsimp(calT(w1, v1, t1)*calT(w2, v2, t2)*calT(w3,v3,t3)*M))


    #Body Frame
    w11 = [0,0,1]
    v11 = [0,a+b,0]

    w22 = [0,0,0]
    v22 = [1,0,0]

    w33 = [0,0,1]
    v33 = [0,b,0]

    pprint(trigsimp(M*calT(w11, v11, t1)*calT(w22, v22, t2)*calT(w33, v33, t3)))


def calScara():
    t4 = Symbol("t4") #theta 4
    t3 = Symbol("t3") #theta 3
    t2 = Symbol("t2") #theta 2
    t1 = Symbol("t1") #theta 1

    a  = Symbol("a")  #a
    b  = Symbol("b")  #b
    c  = Symbol("c")  #c

    M = Matrix([1, 0, 0, 0, \
                0, 1, 0, a+b,   \
                0, 0, 1, c, \
                0, 0, 0, 1]).reshape(4,4)

    w4 = [0,0,0]
    v4 = [0,0,1]

    w3 = [0,0,1]
    v3 = [a+b,0,0]

    w2 = [0,0,1]
    v2 = [a,0,0]

    w1 = [0,0,1]
    v1 = [0,0,0]

    print("exp(s1): ")
    T1 = trigsimp(calT(w1, v1, t1))
    pprint(T1)
    print("J1: ")
    J1 = Matrix([v1[0],v1[1],v1[2],w1[0],w1[1],w1[2]]).reshape(6,1)
    pprint(J1)
    
    print("exp(s2): ")
    T2 = trigsimp(calT(w2, v2, t2))
    pprint(T2)
    print("J2: ")
    J2 = trigsimp(jacob(T1,v2,w2))
    pprint(J2)
    
    print("exp(s3): ")
    T3 = trigsimp(calT(w3, v3, t3))
    pprint(T3)
    print("J3: ")
    J3 = trigsimp(jacob(T1*T2,v3,w3))
    pprint(J3)

    print("exp(s4): ")
    T4 = trigsimp(calT(w4, v4, t4))
    pprint(T4)
    print("J4: ")
    J4 = trigsimp(jacob(T1*T2*T3,v4,w4))
    pprint(J4)

    print("T: ")
    pprint(trigsimp(T1*T2*T3*M))

    print("J: ")
    J = trigsimp(np.hstack((J1,J2,J3,J4)))
    pprint(J)

calScara()