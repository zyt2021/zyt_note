from sympy import *
import numpy as np

init_printing()
t1 = Symbol("t1") #theta 1
t2 = Symbol("t2") #theta 2
t3 = Symbol("t3") #theta 3
t4 = Symbol("t4") #theta 4
t5 = Symbol("t5") #theta 5
t6 = Symbol("t6") #theta 6

d3 = Symbol("d3") #theta d3
d4 = Symbol("d4") #theta d4

a2 = Symbol("a2") #theta a2
a3 = Symbol("a3") #theta a3

T1_0 = np.reshape([cos(t1),-sin(t1),0,0,\
                   sin(t1),cos(t1),0,0,\
                   0,      0,      1,0,\
                   0,      0,      0,1],(4,4))

T2_1 = np.reshape([cos(t2),-sin(t2),0,0,\
                   0,      0,       1,0,\
                  -sin(t2),-cos(t2),0,0,\
                   0,      0,       0,1],(4,4))

T3_2 = np.reshape([cos(t3),-sin(t3),0,a2,\
                   sin(t3),cos(t3),0,0,\
                   0,      0,      1,d3,\
                   0,      0,      0,1],(4,4))

T4_3 = np.reshape([cos(t4),-sin(t4),0,a3,\
                   0,      0,       1,d4,\
                  -sin(t4),-cos(t4),0,0,\
                   0,      0,       0,1],(4,4))

T5_4 = np.reshape([cos(t5),-sin(t5),0,0,\
                   0,      0,      -1,0,\
                   sin(t5), cos(t5),0,0,\
                   0,      0,       0,1],(4,4))

T6_5 = np.reshape([cos(t6),-sin(t6),0,0,\
                   0,      0,       1,0,\
                  -sin(t6),-cos(t6),0,0,\
                   0,      0,       0,1],(4,4))


T6_4 = trigsimp(Matrix(T5_4)*Matrix(T6_5))
T6_3 = trigsimp(Matrix(T4_3)*Matrix(T6_4))
T3_1 = trigsimp(Matrix(T2_1)*Matrix(T3_2))
T6_1 = trigsimp(T3_1*T6_3)
T6_0 = trigsimp(Matrix(T1_0)*T6_1)
print(T6_0)

