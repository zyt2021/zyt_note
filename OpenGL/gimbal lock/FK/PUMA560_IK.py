from sympy import *
import numpy as np

init_printing()
t1 = Symbol("t1") #theta 1
t2 = Symbol("t2") #theta 2
t3 = Symbol("t3") #theta 3
t4 = Symbol("t4") #theta 4
t5 = Symbol("t5") #theta 5
t6 = Symbol("t6") #theta 6

d3 = Symbol("d3") #theta d3
d4 = Symbol("d4") #theta d4

a2 = Symbol("a2") #theta a2
a3 = Symbol("a3") #theta a3

r11 = Symbol("r11") #r11
r12 = Symbol("r12") #r12
r13 = Symbol("r13") #r13

r21 = Symbol("r21") #r21
r22 = Symbol("r22") #r22
r23 = Symbol("r23") #r23

r31 = Symbol("r31") #r31
r32 = Symbol("r32") #r32
r33 = Symbol("r33") #r33

px = Symbol("px") #px
py = Symbol("py") #py
pz = Symbol("pz") #pz


T1_0 = np.reshape([cos(t1),-sin(t1),0,0,\
                   sin(t1),cos(t1),0,0,\
                   0,      0,      1,0,\
                   0,      0,      0,1],(4,4))

T2_1 = np.reshape([cos(t2),-sin(t2),0,0,\
                   0,      0,       1,0,\
                  -sin(t2),-cos(t2),0,0,\
                   0,      0,       0,1],(4,4))

T3_2 = np.reshape([cos(t3),-sin(t3),0,a2,\
                   sin(t3),cos(t3),0,0,\
                   0,      0,      1,d3,\
                   0,      0,      0,1],(4,4))

T4_3 = np.reshape([cos(t4),-sin(t4),0,a3,\
                   0,      0,       1,d4,\
                  -sin(t4),-cos(t4),0,0,\
                   0,      0,       0,1],(4,4))

T5_4 = np.reshape([cos(t5),-sin(t5),0,0,\
                   0,      0,      -1,0,\
                   sin(t5), cos(t5),0,0,\
                   0,      0,       0,1],(4,4))

T6_5 = np.reshape([cos(t6),-sin(t6),0,0,\
                   0,      0,       1,0,\
                  -sin(t6),-cos(t6),0,0,\
                   0,      0,       0,1],(4,4))

T6_4 = trigsimp(Matrix(T5_4)*Matrix(T6_5))
T6_3 = trigsimp(Matrix(T4_3)*Matrix(T6_4))
T3_1 = trigsimp(Matrix(T2_1)*Matrix(T3_2))

# T6_0 = trigsimp(Matrix(T1_0)*T6_1)

T = np.reshape([r11,r12,r13,px,\
                r21,r22,r23,py,\
                r31,r32,r33,pz,\
                0,  0,  0,1],(4,4))

R_base = np.reshape([r11,r12,r13,\
                     r21,r22,r23,\
                     r31,r32,r33],(3,3))

T1_0_INV = np.reshape([cos(t1),sin(t1),0,0,\
                  - sin(t1),cos(t1),0,0,\
                   0,      0,      1,0,\
                   0,      0,      0,1],(4,4))

T6_1_NEW = Matrix(T1_0_INV)*Matrix(T)
T6_1 = trigsimp(T3_1*T6_3)

pprint(T6_1_NEW)
pprint(T6_1)

#theta3
L = (px*cos(t1)+py*sin(t1))*(px*cos(t1)+py*sin(t1))+pz*pz
R = (-a2*sin(t2) - a3*sin(t2 + t3) - d4*cos(t2 + t3))*(-a2*sin(t2) - a3*sin(t2 + t3) - d4*cos(t2 + t3))+\
    (a2*cos(t2) + a3*cos(t2 + t3) - d4*sin(t2 + t3))*(a2*cos(t2) + a3*cos(t2 + t3) - d4*sin(t2 + t3))
E = (-px*sin(t1)+py*cos(t1))*(-px*sin(t1)+py*cos(t1))
pprint(trigsimp(simplify(L+E)))
pprint(trigsimp(simplify(R+d3*d3)))

#theta2 and 4
T3_0 = trigsimp((T1_0*T3_1))
T3_0_INV = T3_0.inverse()
T6_3_NEW = trigsimp(T3_0_INV*Matrix(T))
pprint(T6_3_NEW)
pprint(T6_3)

R3_0_INV = np.reshape([T3_0[0,0],T3_0[1,0],T3_0[2,0],\
                       T3_0[0,1],T3_0[1,1],T3_0[2,1],\
                       T3_0[0,2],T3_0[1,2],T3_0[2,2],],(3,3))

R3_0_NEW = trigsimp(Matrix(R3_0_INV)*Matrix(R_base))
pprint(R3_0_NEW[0,2]) # -c4
pprint(R3_0_NEW[2,2]) # s4


#theta5
T4_0 = trigsimp(Matrix(T1_0)*Matrix(T2_1)*Matrix(T3_2)*Matrix(T4_3))

R4_0_INV = np.reshape([T4_0[0,0],T4_0[1,0],T4_0[2,0],\
                       T4_0[0,1],T4_0[1,1],T4_0[2,1],\
                       T4_0[0,2],T4_0[1,2],T4_0[2,2],],(3,3))

R4_0_NEW = trigsimp(Matrix(R4_0_INV)*Matrix(R_base))

pprint(R4_0_NEW[0,2]) # -s5
pprint(R4_0_NEW[2,2]) # c5

#theta6
T5_0 = trigsimp(Matrix(T1_0)*Matrix(T2_1)*Matrix(T3_2)*Matrix(T4_3)*Matrix(T5_4))
R5_0_INV = np.reshape([T5_0[0,0],T5_0[1,0],T5_0[2,0],\
                       T5_0[0,1],T5_0[1,1],T5_0[2,1],\
                       T5_0[0,2],T5_0[1,2],T5_0[2,2],],(3,3))

R5_0_NEW = trigsimp(Matrix(R5_0_INV)*Matrix(R_base))

pprint(R5_0_NEW[0,0]) # c6
pprint(R5_0_NEW[2,0]) # -s6

