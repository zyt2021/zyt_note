from sympy import *
import numpy as np

a = Symbol("a") #theta 1
b = Symbol("b") #theta 2
c = Symbol("c") #theta 3

Rx = Matrix([1,     0,      0,\
             0,cos(a),-sin(a),\
             0,sin(a),cos(a)]).reshape(3,3)

Ry = Matrix([cos(b),0,sin(b),\
            0,      1,     0,\
            -sin(b),0,cos(b)]).reshape(3,3)

Rz = Matrix([cos(c),-sin(c),0,\
             sin(c), cos(c),0,\
                  0,      0,1]).reshape(3,3)

pprint(trigsimp(Rx*Ry*Rz)) #x-y-z, also x-z-y, y-x-z, y-z-x, z-x-y, z-y-x
