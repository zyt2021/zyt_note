QT -= gui

CONFIG += c++11 console
CONFIG -= app_bundle

# The following define makes your compiler emit warnings if you use
# any Qt feature that has been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
        BaseRender.cpp \
        Shader.cpp \
        main.cpp

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

HEADERS += \
    BaseRender.h \
    Shader.h

INCLUDEPATH += D:/libs/glew-2.1.0/include\
D:/libs/GLFW/include

LIBS+= D:/libs/glew-2.1.0/lib/Release/x64/glew32.lib\
$$quote(C:/Program Files (x86)/Windows Kits/10/Lib/10.0.17763.0/um/x64/OpenGL32.Lib)\
D:/libs/GLFW/lib/glfw3dll.lib
