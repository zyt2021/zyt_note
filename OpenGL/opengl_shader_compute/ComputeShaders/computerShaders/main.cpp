﻿#include <iostream>
#include "BaseRender.h"
#include <Windows.h>

int main() {
    BaseRender::Param param;
    param.height = 600;
    param.width = 600;
    param.vertex_shader = "../glsl/v.glsl";
    param.fragment_shader = "../glsl/f.glsl";

    BaseRender* render = new BaseRender(param);
    render->Initialize();
    bool flag = true;
    {
        render->RunComputeShader();
        render->Draw();
//        if (flag) {
//            getchar();
//            flag = false;
//        }
          //Sleep(1);
//        if (render->Step()) break;
    }
    delete render;
    return 0;
}
